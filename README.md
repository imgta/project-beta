# CarCar

Team:

* Person 1 - Which microservice?
    Gordon Ta - SALES microservice

* Person 2 - Which microservice?
* Person 2 Kapil Adhikari-  Service microservice?

## Design

## Service microservice
## Kapil Adhikari

Explain your models and integration with the inventory
microservice, here.
1. Installed Django app into django project settings:
 Added this line "service_rest.apps.ShoesApiConfig" to INSTALLED_APPS,
2. Created the models: AutomobileVO, Technicians and appointments that are classes I made in models.
3. Developed view functions and encoders.
4. Added path to url.py, admin.py, and shoes_project/urls.py.
5. Launched Insomnia, entered the URLs, and tested the functions.
6. Implemented a polling feature to allow inventory data to interact with services. On insomnia, I put it to the test by adding a new technicians and appointments to my appointmentlist and technicianslist.
7. Begun creating the frontend, introduced the appointment and techniciansList component, implemented cards and columns, with three columns in each row.
8. Added appointment and technicians Form component, wrote jsx as return, gathered input data, and turned it into component states.
9. A nested route has been added to the App component.

## Sales microservice
## GORDON TA
Explain your models and integration with the inventory
microservice, here.

-Installed app (settings.py)
-Created Salesperson, Customer, Sale models as well as a value object model (AutomobileVO) model to link to the existing Automobile model in the Inventory microservice via VIN.
-Created a polling function to fetch automobile data from the Inventory API every 30 seconds.
-Created view functions and encoders for each model in order to manipulate (create, update, delete) and display backend data efficiently and properly (views.py, encoders.py)
-Routed the view functions to their appropriate API URL endpoints paths (urls.py)
-Updated allowed hosts, CORS, etc to enable functional requests between API endpoints from different microservices
-Initialized my backend database by setting up RESTful API HTTP requests using the REST Client extension (requests.http) to generate sales reps, customers, sales, etc.
-Coordinated data fetches from sales, sales reps, customers, and automobile API endpoints to establish sales list, sales form, and sales history. Sales can only be created with unsold cars, sales list only displays sold cars, sales history can filter sales list by sales rep. All changes made are propagated back to the appropriate microservice backend data set.
-Added some extra functionality like sales rep total sales, automatic card/logo creation for manufacturers, shortened models list with pics on hover, auto-fill employee id field, random + relevant form pictures, etc.
