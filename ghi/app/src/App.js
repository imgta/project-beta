import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import RepList from './Sales/RepList';
import RepForm from './Sales/RepForm';
import CustomerList from './Sales/CustomerList';
import CustomerForm from './Sales/CustomerForm';
import SalesList from './Sales/SalesList';
import SalesForm from './Sales/SalesForm';
import RepSalesHistory from './Sales/RepSalesHistory';
import ManufacturersList from './Inventory/ManufacturersList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import AppointmentForm from './Service/AppointmentForm';
import AppointmentList from './Service/AppointmentList';
import TechnicianForm from './Service/TechnicianForm';
import TechnicianList from './Service/TechnicianList';
import SearchAppointment from './Service/SearchAppointment';
import CarModelsList from './Inventory/CarModelsList';
import CarModelForm from './Inventory/CarModelForm';
import AutomobileCreateForm from './Inventory/AutomobileCreateForm';
import AutomobileList from './Inventory/AutomobileList';


function App({reps, customers, sales, manufacturers, models}) {

  reps = reps || [];
  customers = customers || [];
  sales = sales || [];



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="automobiles">
            <Route path="" element={<AutomobileList />} />
            <Route path="new" element={<AutomobileCreateForm />} />
          </Route>

          <Route path="salespeople">
            <Route path="" element={<RepList reps={reps} />} />
            <Route path="create" element={<RepForm />} />
          </Route>

          <Route path="customers">
            <Route path="" element={<CustomerList customers={customers} />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>

          <Route path="sales">
            <Route path="" element={<SalesList sales={sales} />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="history" element={<RepSalesHistory />} />
          </Route>

          <Route path="manufacturers">
            <Route path="" element={<ManufacturersList manufacturers={manufacturers} />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>

          <Route path="models">
            <Route path="" element={<CarModelsList models={models} />} />
            <Route path="create" element={<CarModelForm />} />
          </Route>

          <Route path="services">
            <Route path="technicians">
            <Route path='' element={<TechnicianList />} />
              <Route path='new' element={<TechnicianForm />} />
            </Route>
            <Route path="appointments">
              <Route path="" element={<AppointmentList />} />
              <Route path="new" element={<AppointmentForm />} />
              <Route path='search' element={<SearchAppointment />} />
            </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
