import { NavLink, Link } from 'react-router-dom';
import salesrep from './images/salesrep.jpg';
import customer from './images/customer.jpg';
import records from './images/records.jpg';
import './utils/navBar.js';
import './css/nav.css';
import './css/navlinks.css';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">

            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>

            <li className="nav-item dropdown">
                <NavLink className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Sales  <b className="caret"></b>
                </NavLink>
                <ul className="dropdown-menu mega-menu" aria-labelledby="navbarDropdown">
                    <li className="mega-menu-column">
                        <ul>
                            <li className="nav-header"><h6>Representatives</h6></li>
                            <img src={salesrep} alt="" style={{width: 150, height: 120, objectFit: 'cover'}}/>
                            <li><NavLink to="/salespeople/create">
                            <a class="link anim-up">Add Sales Rep {"\u00A0"}</a>
                            </NavLink></li>
                            <li><NavLink to="/salespeople">
                            <a class="link anim-up">List Sales Reps</a>
                            </NavLink></li>
                            <li><NavLink to="/sales/history">
                            <a class="link anim-up">Sales History</a>
                              </NavLink></li>
                        </ul>
                        </li>

                        <li className="mega-menu-column">
                        <ul>
                        <li className="nav-header"><h6>Customers</h6></li>
                            <img src={customer} alt="" style={{width: 150, height: 120, objectFit: 'cover'}}/>
                            <li><NavLink to="/customers/create">
                            <a class="link anim-up">Add Customer</a>
                            </NavLink></li>
                            <li><NavLink to="/customers">
                            <a class="link anim-up">List Customers</a>
                            </NavLink></li>
                        </ul>
                        </li>

                        <li className="mega-menu-column">
                        <ul>
                        <li className="nav-header"><h6>Sales Records</h6></li>
                            <img src={records} alt="" style={{width: 150, height: 120, objectFit: 'cover'}}/>
                            <li><NavLink to="/sales/create">
                            <a class="link anim-up">Add Sales</a>
                            </NavLink></li>
                            <li><NavLink to="/sales">
                            <a class="link anim-up">List Sales</a>
                            </NavLink></li>
                        </ul>
                        </li>
                    </ul>
                </li>

                <li className="dropdown nav-item">
              <a className="nav-link dropdown-toggle" id="navbarDarkDropdownMenuLink" role="button"
                data-bs-toggle="dropdown" aria-expanded="false">
                    Inventory
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
                <li className="dropdown-item nav-link">
                  <NavLink className="active text-black text-decoration-none" to="/manufacturers">
                    Manufacturers
                  </NavLink>
                </li>
                <li className="dropdown-item nav-link">
                  <NavLink className="active text-black text-decoration-none" to="/manufacturers/create">
                    Add Manufacturers
                  </NavLink>
                </li>
                <li className="dropdown-item nav-link">
                  <NavLink className="active text-black text-decoration-none" to="/models">
                    Models
                  </NavLink>
                </li>
                <li className="dropdown-item nav-link">
                  <NavLink className="active text-black text-decoration-none" to="/models/create">
                    Add Models
                  </NavLink>
                </li>
                <li className="dropdown-item nav-link">
                  <NavLink className="active text-black text-decoration-none" to="/automobiles">
                    Automobiles
                  </NavLink>
                </li>
                <li className="dropdown-item nav-link">
                  <NavLink className="active text-black text-decoration-none" to="/automobiles/new">
                    Add Automobiles
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="dropdown nav-item">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button"
                data-bs-toggle="dropdown" aria-expanded="false">
                Services
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
                <li className="dropdown-item nav-link">
                  <NavLink className="active text-black text-decoration-none" to="services/appointments">
                    Service Appointments List
                  </NavLink>
                </li>
                <li className="dropdown-item nav-link">
                  <NavLink
                    className="active text-black text-decoration-none"
                    to="services/appointments/new">
                    Create an Appointment
                  </NavLink>
                </li>
                <li className="dropdown-item nav-link">
                  <NavLink
                    className="active text-black text-decoration-none"
                    to="services/appointments/search">
                    Search for Appointment
                  </NavLink>
                </li>

                <li className="dropdown-item nav-link">
                  <NavLink
                    className="active text-black text-decoration-none"
                    to="services/technicians/new">
                    Create a Technician
                  </NavLink>
                </li>
                <li className="dropdown-item nav-link">
                  <NavLink
                    className="active text-black text-decoration-none"
                    to="services/technicians">
                    Technician List
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
