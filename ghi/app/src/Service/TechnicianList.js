import { React, useEffect, useState } from "react";

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const technicianResponse = await fetch(technicianUrl);

        if (technicianResponse.ok) {
            const data = await technicianResponse.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
            <h2 className="text-center">Technicians</h2>
            <table className="table-fill">
            <thead>
                <tr>
                    <th className="text-left">Name</th>
                    <th className="text-left">Employee Number</th>
                </tr>
            </thead>
            <tbody className="table-hover">
                {technicians.map(technician => {
                return(
                    <tr key={technician.id}>
                        <td className="text-left">{technician.name}</td>
                        <td className="text-left">{technician.employee_number}</td>
                    </tr>
                );
            })}
            </tbody>
            </table>
        </div>
    )
}

export default TechnicianList
