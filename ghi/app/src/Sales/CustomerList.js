import React, {useEffect, useState} from 'react';


function CustomerList() {
    const [customers, setCustomers] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const url = 'http://localhost:8090/api/customers';
                const response = await fetch(url);

                if (response.ok) {
                    const data = await response.json();
                    setCustomers(data.customers);
                }
            } catch (error) {
                console.error(error);
            }
        };

        fetchData();
    }, []);

    return (
        <>
            <h2 className="text-center">Customers</h2>
        <table className="table-fill">
        <thead>
        <tr>
            <th className="text-left">First Name</th>
            <th className="text-left">Last Name</th>
            <th className="text-left">Phone Number</th>
            <th className="text-left">Address</th>
        </tr>
        </thead>
        <tbody className="table-hover">
            {customers.map(customer => {
                return (
                <tr key={ customer.id }>
                    <td className="text-left">{ customer.first_name }</td>
                    <td className="text-left">{ customer.last_name }</td>
                    <td className="text-left">{ customer.phone_number }</td>
                    <td className="text-left">{ customer.address }</td>
                </tr>
                );
            })}
        </tbody>
    </table>
    </>
    );
}

export default CustomerList;
