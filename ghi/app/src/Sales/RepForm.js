import React, {useState} from 'react';

function RepForm() {

    const [formData, setData] = useState({
        first_name: "",
        last_name: "",
        employee_id: "",
    });

    const handleChange = (event) => {
        const { name, value } = event.target;

        setData(oldData => ({
            ...oldData,
            [name]: value,
        }));

    }

    // Auto-completes "employee_id" field when first and last name fields are populated, on last name field deselect
    const handleBlur = (event) => {
        const { name, value } = event.target;
        setData(oldData => ({
            ...oldData,
            [name]: value,
        }));
        if(formData.first_name && formData.last_name) {
            const lastName = formData.last_name.toLowerCase();
            const firstName = formData.first_name.toLowerCase();
            const employeeId = firstName.charAt(0) + lastName;
            setData(oldData => ({
                ...oldData,
                employee_id: employeeId,
            }));
        };
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData};

        try {
            const repUrl = "http://localhost:8090/api/salespeople/";
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch(repUrl, fetchConfig);
            if(!response.ok) {
                throw new Error('Request for sales reps failed.')
            }
            const newRep = await response.json();

            setData({
                first_name: "",
                last_name: "",
                employee_id: "",
            });

    } catch (error) {
        console.error(error);
    }
    };


    return (
        <div className="row mt-5">
        <div className="col col-sm-auto">
            <img width="350" className="bg-white rounded shadow d-block mx-auto mb-2 pt-4" src="https://source.unsplash.com/350x350/?business" alt="Banner" />
        </div>

        <div className="offset-0 col-8">
            <h2 className="text-center pb-2">Add a Sales Rep</h2>
            <form onSubmit={handleSubmit} id="create-rep-form">
                <div className="form-floating mb-3">
                    <input value={formData.first_name} onBlur={handleBlur} onChange={handleChange} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="firstName">First name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.last_name} onBlur={handleBlur} onChange={handleChange} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="lastName">Last name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.employee_id} onChange={handleChange} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                    <label htmlFor="employeeId">Employee ID</label>
                </div>
                <div id="button-pull">
                <button className="btn btn-primary btn-block">Create</button>
                </div>
            </form>
            </div>
        </div>
    );
}

export default RepForm;
