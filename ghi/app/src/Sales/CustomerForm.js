import React, {useState} from 'react';

function CustomerForm() {
    const [formData, setData] = useState({
        first_name: "",
        last_name: "",
        address: "",
        phone_number: "",
    });

    const handleChange = (event) => {
        const { name, value } = event.target;

        setData(oldData => ({
            ...oldData,
            [name]: value,
        }));
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData};

        try {
            const url = "http://localhost:8090/api/customers/";
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch(url, fetchConfig);
            if(!response.ok) {
                throw new Error('Request for customers failed.')
            }
            const newCustomer = await response.json();

            setData({
                first_name: "",
                last_name: "",
                address: "",
                phone_number: "",
            });

    } catch (error) {
        console.error(error);
    }
    };

    return (
        <div className="row mt-5">
        <div className="col col-sm-auto">
            <img width="350" className="bg-white rounded shadow d-block mx-auto mb-2 pt-4" src="https://source.unsplash.com/350x350/?driver" alt="Banner" />
        </div>

        <div className="offset-0 col-8">
            <h2 className="text-center pb-2">Add a Customer</h2>
            <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                    <input value={formData.first_name} onChange={handleChange} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="firstName">First name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.last_name} onChange={handleChange} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="lastName">Last name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.address} onChange={handleChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.phone_number} onChange={handleChange} placeholder="Phone" required type="text" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="phone_number">Phone number</label>
                </div>
                <div id="button-pull">
                <button className="btn btn-primary btn-block">Create</button>
                </div>
            </form>
            </div>
        </div>
    );
}

export default CustomerForm;
