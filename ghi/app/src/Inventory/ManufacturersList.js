import React, {useEffect, useState} from 'react';
import CardScript from '../utils/cards';
import '../css/cards.css';


function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState([]);
    const [models, setModels] = useState([]);
    const [manufactModels, setManufactModels] = useState({});

    const fetchData = async () => {
        try {
            const url = 'http://localhost:8100/api/manufacturers/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);
            }
        } catch (error) {
            console.error(error);
        }
    };

    const fetchModels = async () => {
        try {
            const url = 'http://localhost:8100/api/models/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setModels(data.models);

                // Group car models by their manufacturer
                const modelManufact = {};
                data.models.forEach(model => {
                    const manufactName = model.manufacturer.name;
                    if(!modelManufact[manufactName]) {
                        modelManufact[manufactName] = [];
                    }
                    modelManufact[manufactName].push(model.name);
                });
                setManufactModels(modelManufact);
            }
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchModels();
        fetchData();
    }, []);


    return (
        <>
            <h2 className="text-center">Automobile Manufacturers</h2>
        <div className="cards">
        {manufacturers.map(manufacturer => {
            const logoName = manufacturer.name.toLowerCase() + ".png";
            const logoUrl = `images/logos/${logoName}`;

        return (
    <div className="card" key={manufacturer.id}>
        <div className="card__image-holder">
            <img className="card__image object-fit-contain" src={logoUrl} alt={manufacturer.name} width="225" height="225" />
        </div>
        <div className="card-title"></div>

        <div className="card-flap flap1">
            <div className="card-description">
            <div className="text-primary fst-italic lh-1 fw-medium">
            </div>
            <br />
            </div>
            <div className="card-flap flap2"></div>
        </div>
    </div>
        );
        })}
        </div>
        <CardScript />
        </>
    );
}


export default ManufacturersList;
