import React, {useEffect, useState} from 'react';
import { Link } from "react-router-dom";
import '../css/hoverlink.css';

function CarModelsList() {
    const [models, setModels] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const url = 'http://localhost:8100/api/models/';
                const response = await fetch(url);

                if (response.ok) {
                    const data = await response.json();
                    setModels(data.models);
                }
            } catch (error) {
                console.error(error);
            }
        };

        fetchData();
    }, []);

    // Group all models by their manufacturer
    const multiModels = models.reduce((accumulate, model) => {
        const { manufacturer } = model;
        if(!accumulate[manufacturer.id]) {
            accumulate[manufacturer.id] = {
                manufacturer,
                models: [],
            };
        }
        accumulate[manufacturer.id].models.push(model);
        return accumulate;
    }, {});



return (
    <>
        <h2 className="text-center">Vehicle Models</h2>
    <table className="table-fill">
    <thead>
    <tr>
        <th className="text-left">Manufacturer</th>
        <th className="text-left">Models</th>
    </tr>
    </thead>
    <tbody className="table-hover">
        {Object.values(multiModels).map(({ manufacturer, models }) => (
            <tr key={manufacturer.id}>
                <td className="text-left w-25">{manufacturer.name}</td>
                <td className="text-left w-25">
                <div className="modelWrap">
                {models.map((model, index) => (
                    <span key={model.id}>
                    <Link to="#">{model.name}
                    <span><img style={{width: 200, height: 150, objectFit: 'contain'}}
                    src={model.picture_url} alt="" /></span>
                    </Link>
                    {index !== models.length - 1 && ",\u00A0"}
                    </span>
                ))}
                </div>
                </td>
            </tr>
        ))}
        </tbody>
</table>
</>
);
}

export default CarModelsList;
