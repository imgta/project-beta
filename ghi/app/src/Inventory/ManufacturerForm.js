import React, {useState} from 'react';

function ManufacturerForm() {

    const [formData, setData] = useState({
        name: "",
    });

    const handleChange = (event) => {
        const { name, value } = event.target;

        setData(oldData => ({
            ...oldData,
            [name]: value,
        }));
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData};

        try {
            const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch(manufacturerUrl, fetchConfig);
            if(!response.ok) {
                throw new Error('Request for sales manufacturers failed.')
            }
            const newManufacturer = await response.json();

            setData({name: "",});

    } catch (error) {
        console.error(error);
    }
    };

    return (
        <div className="row mt-5">
        <div className="col col-sm-auto">
            <img width="350" className="bg-white rounded shadow d-block mx-auto mb-2 pt-4" src="https://source.unsplash.com/350x350/?factory" alt="Banner" />
        </div>

        <div className="offset-0 col-8">
            <h2 className="text-center pb-2">Create a Manufacturer</h2>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
                <div className="form-floating mb-3">
                    <input value={formData.name} onChange={handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Manufacturer name</label>
                </div>
                <div id="button-pull">
                <button className="btn btn-primary btn-block">Create</button>
                </div>
            </form>
            </div>
        </div>
    );

}

export default ManufacturerForm;
