import React, {useEffect, useState} from 'react';

function CarModelForm() {
    const [formData, setData] = useState({
        name: "",
        picture_url: "",
        manufacturers: [],
        manufacturer: "",
    });

    useEffect(() => {
        async function fetchManuData() {
            const url = "http://localhost:8100/api/manufacturers/";
            const response = await fetch(url);
            if(response.ok) {
                const data = await response.json();

                setData((prevData) => ({
                    ...prevData,
                    manufacturers: data.manufacturers
                }));
            }
        }
        fetchManuData();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();

        const data = {
            name: formData.name,
            picture_url: formData.picture_url,
            manufacturer_id: formData.manufacturer
        };

        const modelUrl = "http://localhost:8100/api/models/";
        const fetchCfg = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(modelUrl, fetchCfg);
        if(response.ok) {
            const newModel = await response.json();

            setData({
                name: "",
                picture_url: "",
                manufacturers: formData.manufacturers,
                manufacturer: ""
            });
        }
    }

    function handleChange(event) {
        const { id, value } = event.target;
        setData(prevData => ({
            ...prevData,
            [id]: value
        }));
    }

    return (
        <div className="row mt-5">
        <div className="col col-sm-auto">
            <img width="350" className="bg-white rounded shadow d-block mx-auto mb-2 pt-4" src="https://source.unsplash.com/350x350/?car" alt="Banner" />
        </div>

        <div className="offset-0 col-8">
            <h2 className="text-center pb-2">Create a Vehicle Model</h2>
            <form onSubmit={handleSubmit} id="create-model-form">
                <div className="form-floating mb-3">
                    <input
                    onChange={handleChange}
                    value={formData.name}
                    placeholder="Name"
                    type="text"
                    id="name"
                    className="form-control" />
                    <label htmlFor="name">Model name</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                    onChange={handleChange}
                    value={formData.picture_url}
                    placeholder="Picture"
                    type="text"
                    id="picture_url"
                    className="form-control" />
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select
                    onChange={handleChange}
                    value={formData.manufacturer}
                    required name="manufacturer"
                    id="manufacturer"
                    className="form-select">
                        <option value="">Select manufacturer</option>
                        {formData.manufacturers.map(manufacturer => (
                            <option key={manufacturer.id} value={manufacturer.id}>
                            {manufacturer.name}
                            </option>
                        ))}
                    </select>
                </div>
                    <button className="btn btn-primary btn-block">Create</button>
            </form>
            </div>
        </div>
    );
}

export default CarModelForm;
