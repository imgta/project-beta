from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href",]

class SalesRepEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]

class SalesEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "rep", "customer", "price", "id"]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "rep": SalesRepEncoder(),
        "customer": CustomerEncoder(),
    }
    def get_extra_data(self, obj):
        return {
            "automobile": obj.automobile.vin,
            "rep": obj.rep.employee_id,
            "customer": obj.customer.phone_number,
            }
