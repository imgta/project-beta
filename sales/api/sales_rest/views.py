from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale
from .encoders import AutomobileVOEncoder, SalesRepEncoder, CustomerEncoder, SalesEncoder
import json

# Create your views here.

@require_http_methods(["GET","POST"])
def api_list_reps(request):
    if request.method == "GET":
        reps = Salesperson.objects.all()
        return JsonResponse(
            {"reps": reps},
            encoder=SalesRepEncoder,
        )
    else:
        content = json.loads(request.body)
        reps = Salesperson.objects.create(**content)
        return JsonResponse(
            reps,
            encoder=SalesRepEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_rep(request, pk):
    if request.method == "GET":
        rep = Salesperson.objects.get(id=pk)
        return JsonResponse(
            {"rep": rep},
            encoder = SalesRepEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(id=pk).update(**content)
        rep = Salesperson.objects.get(id=pk)
        return JsonResponse(
            rep,
            encoder=SalesRepEncoder,
            safe=False
        )


@require_http_methods(["GET","POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(
            customers,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            {"customer": customer},
            encoder = CustomerEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except ObjectDoesNotExist:
            return JsonResponse(
                {"error": f"Customer ID {pk} does not exist."},
                status=404
            )
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )


@require_http_methods(["GET","POST"])
def api_list_sales(request, employee_id=None):
    if request.method == "GET":
        if employee_id is None:
            sales = Sale.objects.all()
        else:
            sales = Sale.objects.filter(employee_id=employee_id)
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
            safe=False)

    else:
        content = json.loads(request.body)
        automobile = content["automobile"]
        customer = content["customer"]
        rep = content["rep"]
        try:
            # content["automobile"] = AutomobileVO.objects.get(import_href=content["automobile"])
            content["automobile"] = AutomobileVO.objects.get(vin=content.get("automobile"))
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                # {"message": "Invalid automobile import_href."},
                {"message": "Invalid automobile vin."},
                status = 400)

        try:
            content["rep"] = Salesperson.objects.get(employee_id=content.get("rep"))
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee_id."},
                status=400)

        try:
            content["customer"] = Customer.objects.get(id=content.get("customer"))
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id."},
                status=400)

        record = Sale.objects.create(**content)
        AutomobileVO.objects.filter(vin=automobile).update(sold=True)

        return JsonResponse(
            record,
            encoder=SalesEncoder,
            safe=False,)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                {"sale": sale},
                encoder=SalesEncoder,
                safe=False)
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sales record does not exist."},
                status=404)

    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else: # "PUT"
        content = json.loads(request.body)
        try:
            if "automobile" in content:
                automobile = AutomobileVO.objects.get(vin=content["automobile"])
                content["automobile"] = automobile

            if "rep" in content:
                rep = Salesperson.objects.get(employee_id=content["rep"])
                content["rep"] = rep

            if "customer" in content:
                customer = Customer.objects.get(id=content["customer"])
                content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id."},
                status=400,
            )

        Sale.objects.filter(id=pk).update(**content)
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SalesEncoder,
            safe=False
        )

@require_http_methods(["GET"])
def api_list_autos(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            autos,
            encoder=AutomobileVOEncoder,
            safe=False,
        )
